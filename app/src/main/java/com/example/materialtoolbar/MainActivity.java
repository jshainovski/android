package com.example.materialtoolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;
import butterknife.BindView;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.materialtoolbar.adapter.MyAdapter;
import com.example.materialtoolbar.adapter.MyVideoAdapter;
import com.example.materialtoolbar.adapter.ViewPageAdapter;
import com.example.materialtoolbar.behavior.CardFragment;
import com.example.materialtoolbar.db.DaoManagerImpl;
import com.example.materialtoolbar.model.Video;
import com.example.materialtoolbar.model.localita.Comuni;
import com.example.materialtoolbar.model.user.Account;
import com.example.materialtoolbar.model.Model;
import com.example.materialtoolbar.model.user.Anagrafica;
import com.example.materialtoolbar.model.user.Localita;
import com.example.materialtoolbar.model.user.Media;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

		private static final String TAG = "MainActivity";
		private static final int REQUEST_TAKE_GALLERY_VIDEO = 1;

		@BindView(R.id.user_profile_photo)
		com.mikhaellopez.circularimageview.CircularImageView imgProfile;
		public static final int REQUEST_IMAGE = 100;


		private static final int MY_PERMISSIONS_REQUEST_INTERNET = 1;
		private Toolbar toolbar;
		private boolean isShowingCardHeaserShasow;
		private MyAdapter myAdapter;
		private MyVideoAdapter myVideoAdapter;
		private EditText edtPassword;
		private AppCompatCheckBox checkbox;
		private androidx.appcompat.widget.AppCompatImageView imageViewSee;
		List<Model> modelList = new ArrayList<>();
		List<Video> videoList = new ArrayList<>();
		private Account account;
		private String imgSfondoOrProfilo;
		private String insertVideoPathSalva;

		private MediaController mediaController;
		private VideoView videoView;

		@Override
		protected void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				setContentView(R.layout.login);
				
				mediaController = new MediaController(this);

				checkbox = findViewById(R.id.VisPassword);
				edtPassword = findViewById(R.id.txtPassword);
				imageViewSee = findViewById(R.id.ImgPassword);
				checkbox.setOnCheckedChangeListener((compoundButton, isChecked) -> {
						if (!isChecked) {
								// show password
								imageViewSee.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_hide));
								edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
						} else {
								// hide password
								imageViewSee.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_see));
								edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
						}
				});


		}

		private ViewPageAdapter createCardAdapter(Account acc, boolean firstTime) {
				ViewPageAdapter adapter = new ViewPageAdapter(this, this, acc, firstTime);
				return adapter;
		}

		private void showOrhideView(View view, boolean isShow) {
				view.animate().alpha(isShow ? 1f : 0f).setDuration(100).setInterpolator(new DecelerateInterpolator());
		}

		private void generateModelList() throws SQLException {
				modelList.clear();
				Dao<Account, String> accountDao = DaoManagerImpl.getAccountDao(this);

				QueryBuilder<Account, String> accountUsers = accountDao.queryBuilder();

				accountUsers.where().ne("id", account.getId());

				List<Account> accounts = accountUsers.query();

				for (int i = 0; i < accounts.size(); i++) {
						Account acc = accounts.get(i);
						Dao<Media, String> mediaDao = DaoManagerImpl.getMediaDao(this);
						Dao<Localita, String> localitaDao = DaoManagerImpl.getLocalitaDao(this);

						//carico imagine sfondo
						Map<String, Object> map = new HashMap<String, Object>() {{
								put("idTab", acc.getId());
								put("nome", "sfondoProfilo");
								put("descrizione", "sfondoProfilo");
						}};
						List<Media> media = mediaDao.queryForFieldValues(map);
						Log.d(TAG, "Login: " + media.size());
						String url = "";
						if (media.size() >= 1) {
								Media med = media.get(0);
								url = med.getPath();
						}
						//carico foto profilo
						Map<String, Object> map2 = new HashMap<String, Object>() {{
								put("idTab", acc.getId());
								put("nome", "fotoProfilo");
								put("descrizione", "fotoProfilo");
						}};
						List<Media> media2 = mediaDao.queryForFieldValues(map2);
						Log.d(TAG, "Login: " + media.size());
						String urlPath = "";
						if (media2.size() >= 1) {
								Media med2 = media2.get(0);
								urlPath = med2.getPath();
						}

						List<Localita> localita = localitaDao.queryForEq("idTab", acc.getId());
						String citta = "";
						if (localita.size() >= 1) {
								Localita loc = localita.get(0);
								citta = loc.getCitta();
						}


						modelList.add(new Model(url, urlPath, acc.getUsername(), citta));
				}

		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
				getMenuInflater().inflate(R.menu.menu_main, menu);
				return true;
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
				int id = item.getItemId();
				if (id == R.id.action_settings) {
						try {
								DisegnaVideoPage();
						} catch (SQLException e) {
								e.printStackTrace();
						}
				}
				if (id == R.id.action_profilo) {
						setContentView(R.layout.profile);
						try {
								DisegnaTab(false);
						} catch (SQLException e) {
								e.printStackTrace();
						} catch (IOException e) {
								e.printStackTrace();
						}
				}

				return super.onOptionsItemSelected(item);
		}

		@Override
		public void onRequestPermissionsResult(int requestCode,
																					 String[] permissions, int[] grantResults) {
				switch (requestCode) {
						case MY_PERMISSIONS_REQUEST_INTERNET: {
								// If request is cancelled, the result arrays are empty.
								if (grantResults.length > 0
												&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
										// permission was granted, yay! Do the
										// contacts-related task you need to do.
								} else {
										// permission denied, boo! Disable the
										// functionality that depends on this permission.
								}
								return;
						}

						// other 'case' lines to check for other
						// permissions this app might request.
				}
		}

		public void Login(View view) {
				EditText tx1 = findViewById(R.id.txtEmail);
				EditText tx2 = findViewById(R.id.txtPassword);

				try {
						Dao<Account, String> accountDao = DaoManagerImpl.getAccountDao(this);

						Account matchObj = new Account();
						matchObj.setUsername(tx1.getText().toString());
						matchObj.setPassword(tx2.getText().toString());

						List<Account> accounts = accountDao.queryForMatching(matchObj);

						Log.d(TAG, "Login: " + accounts.size());

						if (accounts.size() == 1) {
								account = accounts.get(0);

								Dao<Anagrafica, String> anagraficaDao = DaoManagerImpl.getAnagraficaDao(this);
								Dao<Localita, String> localitaDao = DaoManagerImpl.getLocalitaDao(this);

								Long idUser = account.getId();

								List<Anagrafica> anag = anagraficaDao.queryForEq("idTab", idUser);
								List<Localita> localita = localitaDao.queryForEq("idTab", idUser);

								if (anag.size() > 0 && localita.size() > 0) {
										DisegnaHomePage();
								} else {
										DisegnaTab(true);
								}

						} else {
								Toast.makeText(this, "Account non trovato: " + matchObj.getUsername(), Toast.LENGTH_LONG).show();
						}

				} catch (SQLException | IOException e) {
						e.printStackTrace();
				}

		}

		public void SignUp(View view) {
				setContentView(R.layout.signup);
		}

		public void BackFromSignUp(View view) throws SQLException {
				if (account == null) {
						setContentView(R.layout.login);
				} else {
						setContentView(R.layout.activity_main);
						DisegnaHomePage();
				}
		}

		public void SignUpAccount(View view) {
				EditText txtUsername = findViewById(R.id.txtUserName);
				EditText txtEmail = findViewById(R.id.txtEmail);
				EditText txtPassword = findViewById(R.id.txtPassword);
				EditText txtRetryPassword = findViewById(R.id.txtRetypePassword);

				try {
						boolean correct = txtPassword.getText().toString().equals(txtRetryPassword.getText().toString());
						if (correct == false) {
								Toast.makeText(this, "Le password non corrispondono", Toast.LENGTH_LONG).show();
								return;
						}

						Dao<Account, String> accountDao = DaoManagerImpl.getAccountDao(this);

						Account matchObj = new Account();
						matchObj.setUsername(txtUsername.getText().toString());
						//matchObj.setEmail(txtEmail.getText().toString());
						//matchObj.setPassword(txtPassword.getText().toString());

						List<Account> accounts = accountDao.queryForMatching(matchObj);
						Log.d(TAG, "Login: " + accounts.size());

						if (accounts.size() >= 1) {
								Toast.makeText(this, "Username già presente: " + matchObj.getUsername(), Toast.LENGTH_LONG).show();
								return;
						}
						accountDao.create(matchObj);
						Toast.makeText(this, "Creato account correttamente", Toast.LENGTH_LONG).show();

						setContentView(R.layout.login);

           /* List<Account> accounts = accountDao.queryForMatching(matchObj);
            Log.d(TAG, "Login: " + accounts.size());

            if(accounts.size() == 1) {
                Account account = accounts.get(0);
                DisegnaHomePage();
                Toast.makeText(this, "Accesso effettuato: " + account.getEmail(), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Account non trovato: " + matchObj.getUsername(), Toast.LENGTH_LONG).show();
            }*/

				} catch (SQLException e) {
						e.printStackTrace();
				}

		}

		private void DisegnaHomePage() throws SQLException {
				setContentView(R.layout.activity_main);
				toolbar = findViewById(R.id.toolbar);

				setSupportActionBar(toolbar);

				if (ContextCompat.checkSelfPermission(this,
								Manifest.permission.INTERNET)
								!= PackageManager.PERMISSION_GRANTED) {

						// No explanation needed; request the permission
						ActivityCompat.requestPermissions(this,
										new String[]{Manifest.permission.INTERNET},
										MY_PERMISSIONS_REQUEST_INTERNET);

						// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
						// app-defined int constant. The callback method gets the
						// result of the request.
						Toast.makeText(this, "Permission requested", Toast.LENGTH_LONG);
				} else {
						// Permission has already been granted
						Toast.makeText(this, "Permission already granted", Toast.LENGTH_SHORT);
				}

				generateModelList();

				//final MaxHeightRecyclerView rv = findViewById(R.id.card_recycler_view);
				final RecyclerView rv = findViewById(R.id.card_recycler_view);
				final LinearLayoutManager lm = new LinearLayoutManager(this);
				rv.setLayoutManager(lm);
				myAdapter = new MyAdapter(this, modelList);
				rv.setAdapter(myAdapter);
				rv.addItemDecoration(new DividerItemDecoration(this, lm.getOrientation()));
				final View cardHeaderShadow = findViewById(R.id.card_header_view);
				rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
						@Override
						public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
								boolean isRecyclerViewScrolledToTop = lm.findLastVisibleItemPosition() == 0
												&& lm.findViewByPosition(0).getTop() == 0;
								if (!isRecyclerViewScrolledToTop && !isShowingCardHeaserShasow) {
										isShowingCardHeaserShasow = true;
										showOrhideView(cardHeaderShadow, isShowingCardHeaserShasow);
								} else {
										isShowingCardHeaserShasow = false;
										showOrhideView(cardHeaderShadow, isShowingCardHeaserShasow);
								}
						}
				});

				NestedScrollView nestedScrollView = findViewById(R.id.nested_scroll_view);
				nestedScrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
				nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
						@Override
						public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
								if (scrollY == 0 && oldScrollY > 0) {
										rv.scrollToPosition(0);
										cardHeaderShadow.setAlpha(0f);
										isShowingCardHeaserShasow = false;
								}
						}
				});
		}

		private void DisegnaTab(boolean firstTime) throws SQLException, IOException {
				setContentView(R.layout.profile);
				Dao<Media, String> mediaDao = DaoManagerImpl.getMediaDao(this);

				Long idUser = account.getId();

				Map<String, Object> map = new HashMap<String, Object>() {{
						put("idTab", idUser);
						put("nome", "fotoProfilo");
						put("descrizione", "fotoProfilo");
				}};
				List<Media> media = mediaDao.queryForFieldValues(map);
				Log.d(TAG, "Login: " + media.size());

				if (media.size() >= 1) {
						Media med = media.get(0);
						// loading profile image from local cache
						Uri myUri = Uri.parse(med.getPath());
						loadProfile(myUri, "PROFILO");
				}

				Map<String, Object> map2 = new HashMap<String, Object>() {{
						put("idTab", idUser);
						put("nome", "sfondoProfilo");
						put("descrizione", "sfondoProfilo");
				}};
				List<Media> media2 = mediaDao.queryForFieldValues(map2);
				Log.d(TAG, "Login: " + media.size());

				if (media2.size() >= 1) {
						Media med2 = media.get(0);
						// loading profile image from local cache
						Uri myUri = Uri.parse(med2.getPath());
						loadProfile(myUri, "SFONDO");
				}

				TextView txtUsername = findViewById(R.id.lbl_user_username);
				TextView txtEmail = findViewById(R.id.lbl_user_email);

				txtUsername.setText(account.getUsername());
				txtEmail.setText(account.getEmail());

				TabLayout tabLayout;
				ViewPager2 viewPager;

				viewPager = findViewById(R.id.view_pager);
				tabLayout = findViewById(R.id.tabLayout);
				viewPager.setAdapter(createCardAdapter(account, firstTime));

				/*viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
						@Override
						public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

						}

						@Override
						public void onPageSelected(int position) {
								CardFragment.setAnagraficaUser(position);
						}

						@Override
						public void onPageScrollStateChanged(int state) {

						}
				});*/

				tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
						@Override
						public void onTabSelected(TabLayout.Tab tab) {
								//do stuff here

								CardFragment.setAnagraficaUser(tab.getPosition());
						}

						@Override
						public void onTabUnselected(TabLayout.Tab tab) {

						}

						@Override
						public void onTabReselected(TabLayout.Tab tab) {

						}
				});
				new TabLayoutMediator(tabLayout, viewPager,
								new TabLayoutMediator.TabConfigurationStrategy() {
										@Override
										public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {

												if (position == 0) {
														tab.setText("Informazioni personali");

												} else if (position == 1) {
														tab.setText("Località");
												}
										}

								}).attach();
		}


		private void loadProfile(Uri url, String profOrSfondo) throws IOException {
				Log.d(TAG, "Image cache path: " + url);
				try {
						Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), url);
						if (profOrSfondo.equals("PROFILO")) {
								com.mikhaellopez.circularimageview.CircularImageView button1 = (CircularImageView) findViewById(R.id.user_profile_photo);
								if (bitmap != null) {
										button1.setImageBitmap(bitmap);
								}
						} else {
								androidx.appcompat.widget.AppCompatImageView button1 = findViewById(R.id.image_view_sfondo);
								if (bitmap != null) {
										button1.setImageBitmap(bitmap);
								}
						}

				} catch (IOException e) {
						e.printStackTrace();
				}
		}

		private void loadProfileDefault() {
				com.mikhaellopez.circularimageview.CircularImageView button1 = (CircularImageView) findViewById(R.id.user_profile_photo);
				button1.setBackgroundResource(R.drawable.img);
		}

		public void onProfileImageClick(View view) {
				imgSfondoOrProfilo = "PROFILO";
				Activity con = this;

				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
								switch (which) {
										case DialogInterface.BUTTON_POSITIVE:

												Dexter.withActivity(con)
																.withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
																.withListener(new MultiplePermissionsListener() {
																		@Override
																		public void onPermissionsChecked(MultiplePermissionsReport report) {
																				if (report.areAllPermissionsGranted()) {
																						showImagePickerOptions();
																				}

																				if (report.isAnyPermissionPermanentlyDenied()) {
																						showSettingsDialog();
																				}
																		}

																		@Override
																		public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
																				token.continuePermissionRequest();
																		}
																}).check();
										case DialogInterface.BUTTON_NEGATIVE:
												//No button clicked
												break;
								}
						}
				};


				Map<String, Object> map2 = new HashMap<String, Object>() {{
						put("idTab", account.getId());
						put("nome", "fotoProfilo");
						put("descrizione", "fotoProfilo");
				}};


				List<Media> media = null;

				try {
						Dao<Media, String> mediaDao = DaoManagerImpl.getMediaDao(this);
						media = mediaDao.queryForFieldValues(map2);

						if (media.size() >= 1) {
								AlertDialog.Builder builder = new AlertDialog.Builder(this);
								builder.setMessage("Sicuro di voler cambiare immagine profilo?").setPositiveButton("Yes", dialogClickListener)
												.setNegativeButton("No", dialogClickListener).show();
						} else {
								Dexter.withActivity(con)
												.withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
												.withListener(new MultiplePermissionsListener() {
														@Override
														public void onPermissionsChecked(MultiplePermissionsReport report) {
																if (report.areAllPermissionsGranted()) {
																		showImagePickerOptions();
																}
																if (report.isAnyPermissionPermanentlyDenied()) {
																		showSettingsDialog();
																}
														}
														@Override
														public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
																token.continuePermissionRequest();
														}
												}).check();
						}


				} catch (SQLException e) {
						e.printStackTrace();
				}


		}


		private void showImagePickerOptions() {
				ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
						@Override
						public void onTakeCameraSelected() {
								launchCameraIntent();
						}

						@Override
						public void onChooseGallerySelected() {
								launchGalleryIntent();
						}
				});
		}

		private void launchCameraIntent() {
				Intent intent = new Intent(MainActivity.this, ImagePickerActivity.class);
				intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

				// setting aspect ratio
				intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
				intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
				intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

				// setting maximum bitmap width and height
				intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
				intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
				intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

				startActivityForResult(intent, REQUEST_IMAGE);
		}

		private void launchGalleryIntent() {
				Intent intent = new Intent(MainActivity.this, ImagePickerActivity.class);
				intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

				// setting aspect ratio
				intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
				intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
				intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
				startActivityForResult(intent, REQUEST_IMAGE);
		}

		@Override
		protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
				super.onActivityResult(requestCode, resultCode, data);
				if (requestCode == REQUEST_IMAGE) {
						if (resultCode == Activity.RESULT_OK) {

								Uri uri = data.getParcelableExtra("path");
								try {
										// You can update this bitmap to your server
										Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

										Dao<Media, String> mediaDao = DaoManagerImpl.getMediaDao(this);

										Calendar currentTime = Calendar.getInstance();

										int monthInt = (currentTime.get(Calendar.MONTH) + 1);
										String monthConverted = ""+monthInt;
										if(monthInt<10){
												monthConverted = "0"+monthConverted;
										}

										String dataInserimento = currentTime.get(Calendar.DAY_OF_MONTH) + "/" + monthConverted+ "/" + currentTime.get(Calendar.YEAR);
										String oraInserimento = currentTime.get(Calendar.HOUR_OF_DAY) + ":" + currentTime.get(Calendar.MINUTE) + ":" + currentTime.get(Calendar.SECOND);

										Media matchObj = new Media();
										matchObj.setPath(uri.toString());
										matchObj.setIdTab(account.getId());
										matchObj.setData_inserimento(dataInserimento);
										matchObj.setOra_inserimento(oraInserimento);
										Map<String, Object> map2;
										if (imgSfondoOrProfilo.equals("PROFILO")) {
												matchObj.setNome("fotoProfilo");
												matchObj.setDescrizione("fotoProfilo");

												map2 = new HashMap<String, Object>() {{
														put("idTab", account.getId());
														put("nome", "fotoProfilo");
														put("descrizione", "fotoProfilo");
												}};
										} else {
												matchObj.setNome("sfondoProfilo");
												matchObj.setDescrizione("sfondoProfilo");

												map2 = new HashMap<String, Object>() {{
														put("idTab", account.getId());
														put("nome", "sfondoProfilo");
														put("descrizione", "sfondoProfilo");
												}};
										}

										List<Media> media = mediaDao.queryForFieldValues(map2);
										Log.d(TAG, "Login: " + media.size());

										if (media.size() >= 1) {
												UpdateBuilder<Media, String> updateBuilder = mediaDao.updateBuilder();

												updateBuilder.updateColumnValue("path", uri.toString());
												updateBuilder.where().eq("idTab", account.getId()).and().eq("nome", map2.get("nome")).and().eq("descrizione", map2.get("descrizione"));

												updateBuilder.update();
										} else {
												mediaDao.create(matchObj);
										}

										// loading profile image from local cache
										loadProfile(uri, imgSfondoOrProfilo);
								} catch (IOException | SQLException e) {
										e.printStackTrace();
								}
						}

				}
				if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
						if (resultCode == Activity.RESULT_OK) {
								Uri videoUri = data.getData();
								if (videoUri != null){
										playVideoFromURL(videoUri);

								}
						}
				}
		}

		/**
		 * Showing Alert Dialog with Settings option
		 * Navigates user to app settings
		 * NOTE: Keep proper title and message depending on your app
		 */
		private void showSettingsDialog() {
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle(getString(R.string.dialog_permission_title));
				builder.setMessage(getString(R.string.dialog_permission_message));
				builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
						dialog.cancel();
						openSettings();
				});
				builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
				builder.show();

		}

		// navigating user to app settings
		private void openSettings() {
				Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
				Uri uri = Uri.fromParts("package", getPackageName(), null);
				intent.setData(uri);
				startActivityForResult(intent, 101);
		}

		public void onBackgroundClick(View view) {
				imgSfondoOrProfilo = "SFONDO";
				Activity con = this;

				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
								switch (which) {
										case DialogInterface.BUTTON_POSITIVE:

												Dexter.withActivity(con)
																.withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
																.withListener(new MultiplePermissionsListener() {
																		@Override
																		public void onPermissionsChecked(MultiplePermissionsReport report) {
																				if (report.areAllPermissionsGranted()) {
																				}

																				if (report.isAnyPermissionPermanentlyDenied()) {
																						showSettingsDialog();
																				}
																		}

																		@Override
																		public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
																				token.continuePermissionRequest();
																		}
																}).check();
										case DialogInterface.BUTTON_NEGATIVE:
												//No button clicked
												break;
								}
						}
				};


				Map<String, Object> map2 = new HashMap<String, Object>() {{
						put("idTab", account.getId());
						put("nome", "sfondoProfilo");
						put("descrizione", "sfondoProfilo");
				}};


				List<Media> media = null;

				try {
						Dao<Media, String> mediaDao = DaoManagerImpl.getMediaDao(this);
						media = mediaDao.queryForFieldValues(map2);

						if (media.size() >= 1) {
								AlertDialog.Builder builder = new AlertDialog.Builder(this);
								builder.setMessage("Sicuro di voler cambiare l' immagine di sfondo?").setPositiveButton("Yes", dialogClickListener)
												.setNegativeButton("No", dialogClickListener).show();
						} else {
								Dexter.withActivity(con)
												.withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
												.withListener(new MultiplePermissionsListener() {
														@Override
														public void onPermissionsChecked(MultiplePermissionsReport report) {
																if (report.areAllPermissionsGranted()) {
																		showImagePickerOptions();
																}

																if (report.isAnyPermissionPermanentlyDenied()) {
																		showSettingsDialog();
																}
														}

														@Override
														public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
																token.continuePermissionRequest();
														}
												}).check();
						}


				} catch (SQLException e) {
						e.printStackTrace();
				}
		}

		private void setMediaCont(){
				videoView = findViewById(R.id.video_view);
				mediaController.setMediaPlayer(videoView);
				mediaController.setAnchorView(videoView);
				videoView.setMediaController(mediaController);
		}

		private void playVideoFromURL(Uri path){
				ImageView view1 = findViewById(R.id.img_video);
				ImageView view2 = findViewById(R.id.img_video1);
				view1.setVisibility(View.GONE);
				view2.setVisibility(View.GONE);
				setMediaCont();
				videoView.setVisibility(View.VISIBLE);
				videoView.setVideoURI(path);
				insertVideoPathSalva = path.toString();
				videoView.start();
		}

		public void videoPicker (View view){
				Intent intent = new Intent();
				intent.setType("video/*");
				intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
				startActivityForResult(Intent.createChooser(intent,"Select Video"),REQUEST_TAKE_GALLERY_VIDEO);

		}
		public void onVideoClickSalva(View view){
				EditText view1 = findViewById(R.id.txt_video_name);
				EditText view2 = findViewById(R.id.txt_video_descr);

				String nome = view1.getText().toString();
				String descrizione = view2.getText().toString();


				if (insertVideoPathSalva.equals("")){
						Toast.makeText(this, "Non è presente il video", Toast.LENGTH_SHORT);
						return;
				}
				if (nome.equals("")){
						Toast.makeText(this, "Non è presente il nome del video", Toast.LENGTH_SHORT);
						return;
				}
				if (descrizione.equals("")){
						Toast.makeText(this, "Non è presente la descrizione del video", Toast.LENGTH_SHORT);
						return;
				}


				Dao<Media, String> mediaDao = null;
				try {
						mediaDao = DaoManagerImpl.getMediaDao(this);


						Calendar currentTime = Calendar.getInstance();


						int monthInt = (currentTime.get(Calendar.MONTH) + 1);
						String monthConverted = ""+monthInt;
						if(monthInt<10){
								monthConverted = "0"+monthConverted;
						}


						String dataInserimento = currentTime.get(Calendar.DAY_OF_MONTH) + "/" + monthConverted + "/" + currentTime.get(Calendar.YEAR);
						String oraInserimento = currentTime.get(Calendar.HOUR_OF_DAY) + ":" + currentTime.get(Calendar.MINUTE) + ":" + currentTime.get(Calendar.SECOND);

						Media matchObj = new Media();
						matchObj.setPath(insertVideoPathSalva);
						matchObj.setIdTab(account.getId());
						matchObj.setNome(nome);
						matchObj.setDescrizione(descrizione);
						matchObj.setData_inserimento(dataInserimento);
						matchObj.setOra_inserimento(oraInserimento);


						mediaDao.create(matchObj);
						insertVideoPathSalva = "";
						Toast.makeText(this, "Salvataggio effettuato correttamente", Toast.LENGTH_LONG);
						setContentView(R.layout.activity_main);
						DisegnaHomePage();
				} catch (SQLException e) {
						e.printStackTrace();
				}
				InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		}


		public void BackToMyVideo(View view) throws SQLException {
				setContentView(R.layout.my_video_list);
				DisegnaVideoPage();

		}
		public void addVideo (View view){
				setContentView(R.layout.add_video);

		}
		private void DisegnaVideoPage() throws SQLException {
				setContentView(R.layout.my_video_list);

				generateVideoList();

				//final MaxHeightRecyclerView rv = findViewById(R.id.card_recycler_view);
				final RecyclerView rv = findViewById(R.id.card_recycler_view_video);
				final LinearLayoutManager lm = new LinearLayoutManager(this);
				rv.setLayoutManager(lm);
				myVideoAdapter = new MyVideoAdapter(this, videoList);
				rv.setAdapter(myVideoAdapter);
				rv.addItemDecoration(new DividerItemDecoration(this, lm.getOrientation()));
				final View cardHeaderShadow = findViewById(R.id.card_header_view_video);
				rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
						@Override
						public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
								boolean isRecyclerViewScrolledToTop = lm.findLastVisibleItemPosition() == 0
												&& lm.findViewByPosition(0).getTop() == 0;
								if (!isRecyclerViewScrolledToTop && !isShowingCardHeaserShasow) {
										isShowingCardHeaserShasow = true;
										showOrhideView(cardHeaderShadow, isShowingCardHeaserShasow);
								} else {
										isShowingCardHeaserShasow = false;
										showOrhideView(cardHeaderShadow, isShowingCardHeaserShasow);
								}
						}
				});

				NestedScrollView nestedScrollView = findViewById(R.id.nested_scroll_view_video);
				nestedScrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);
				nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
						@Override
						public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
								if (scrollY == 0 && oldScrollY > 0) {
										rv.scrollToPosition(0);
										cardHeaderShadow.setAlpha(0f);
										isShowingCardHeaserShasow = false;
								}
						}
				});
		}

		private void generateVideoList() throws SQLException {
				videoList.clear();
				Dao<Media, String> mediaDao = DaoManagerImpl.getMediaDao(this);

				QueryBuilder<Media, String> mediaVideo = mediaDao.queryBuilder();

				Where<Media, String> where = mediaVideo.where();
				where.and(
								where.and(
												where.ne("nome", "sfondoProfilo"),
												where.ne("descrizione", "sfondoProfilo")),
								where.and(
												where.ne("nome", "fotoProfilo"),
												where.ne("descrizione", "fotoProfilo")));

				List<Media> media = mediaVideo.query();

				for (int i = 0; i < media.size(); i++) {
						Media med = media.get(i);
						videoList.add(new Video(med.getPath(), med.getNome(), med.getDescrizione()));
				}

		}




}
