package com.example.materialtoolbar.db;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DatabaseInitializer extends SQLiteAssetHelper {

	private static final String TAG = "DatabaseInitializer";

	private static final String DATABASE_NAME = "Database.db";
	private static final int DATABASE_VERSION = 1;
	private File DATABASE_FILE;

	private static Context context;
	private static DatabaseInitializer sInstance;
	private static ConnectionSource connectionSource;

	public static synchronized DatabaseInitializer getInstance(Context context) {
		if (sInstance == null)  sInstance = new DatabaseInitializer(context);
		return sInstance;
	}

	/**
	 * Constructor should be private to prevent direct instantiation.
	 * Make a call to the static method "getInstance()" instead.
	 */
	private DatabaseInitializer(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		connectionSource = new AndroidConnectionSource(this);

		SQLiteDatabase db = this.getWritableDatabase();
		Log.d(TAG, "Database version: " + db.getVersion()); // what value do you get here?

		this.context = context;
		DATABASE_FILE = this.context.getDatabasePath(DATABASE_NAME);
		Log.e("Database path", DATABASE_FILE.getAbsolutePath());
	}

	// Called when the database needs to be upgraded.
	// This method will only be called if a database already exists on disk with the same DATABASE_NAME,
	// but the DATABASE_VERSION is different than the version of the database that exists on disk.
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion != newVersion) {
			try {
				copyDataBase();
				Log.e(TAG, "createDatabase database created");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// Copy the database from assets
	private void copyDataBase() throws IOException {
		InputStream mInput = context.getAssets().open(DATABASE_NAME);
		OutputStream mOutput = new FileOutputStream(DATABASE_FILE);
		byte[] mBuffer = new byte[1024];
		int mLength;
		while ((mLength = mInput.read(mBuffer)) > 0) {
			mOutput.write(mBuffer, 0, mLength);
		}
		mOutput.flush();
		mOutput.close();
		mInput.close();
	}

	public ConnectionSource getConnectionSource() {
		return connectionSource;
	}
}
