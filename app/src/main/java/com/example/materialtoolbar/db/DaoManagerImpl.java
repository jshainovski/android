package com.example.materialtoolbar.db;

import android.content.Context;

import com.example.materialtoolbar.model.user.Account;
import com.example.materialtoolbar.model.localita.Comuni;
import com.example.materialtoolbar.model.localita.Province;
import com.example.materialtoolbar.model.localita.Regioni;
import com.example.materialtoolbar.model.user.Anagrafica;
import com.example.materialtoolbar.model.user.Localita;
import com.example.materialtoolbar.model.user.Media;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

import java.sql.SQLException;

public class DaoManagerImpl {

    public static Dao<Account, String> getAccountDao(Context context) throws SQLException {
		DatabaseInitializer dbInstance = DatabaseInitializer.getInstance(context);
        return DaoManager.createDao(dbInstance.getConnectionSource(), Account.class);
    }
		public static Dao<Regioni, String> getRegioniDao(Context context) throws SQLException {
				DatabaseInitializer dbInstance = DatabaseInitializer.getInstance(context);
				return DaoManager.createDao(dbInstance.getConnectionSource(), Regioni.class);
		}
		public static Dao<Province, String> getProvinceDao(Context context) throws SQLException {
				DatabaseInitializer dbInstance = DatabaseInitializer.getInstance(context);
				return DaoManager.createDao(dbInstance.getConnectionSource(), Province.class);
		}
		public static Dao<Comuni, String> getComuniDao(Context context) throws SQLException {
				DatabaseInitializer dbInstance = DatabaseInitializer.getInstance(context);
				return DaoManager.createDao(dbInstance.getConnectionSource(), Comuni.class);
		}
		public static Dao<Anagrafica, String> getAnagraficaDao(Context context) throws SQLException {
				DatabaseInitializer dbInstance = DatabaseInitializer.getInstance(context);
				return DaoManager.createDao(dbInstance.getConnectionSource(), Anagrafica.class);
		}
		public static Dao<Localita, String> getLocalitaDao(Context context) throws SQLException {
				DatabaseInitializer dbInstance = DatabaseInitializer.getInstance(context);
				return DaoManager.createDao(dbInstance.getConnectionSource(), Localita.class);
		}
		public static Dao<Media, String> getMediaDao(Context context) throws SQLException {
				DatabaseInitializer dbInstance = DatabaseInitializer.getInstance(context);
				return DaoManager.createDao(dbInstance.getConnectionSource(), Media.class);
		}
}
