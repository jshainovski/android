package com.example.materialtoolbar.video;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.example.materialtoolbar.R;
import com.example.materialtoolbar.adapter.MyVideoAdapter;
import com.example.materialtoolbar.video.FullScreenMediaController;

import androidx.appcompat.app.AppCompatActivity;

public class Video1 extends AppCompatActivity {

		private VideoView videoView;
		private VideoView videoViewBackground;
		private MediaController mediaController;
		@SuppressLint("SourceLockedOrientationActivity")
		@Override
		protected void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				setContentView(R.layout.videoview_fullscreen);
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

				RelativeLayout r = findViewById(R.id.full_video);

				Intent intent = getIntent();
				String pathVideo = intent.getStringExtra(MyVideoAdapter.EXTRA_MESSAGE);


				videoView = findViewById(R.id.videoViewRelative);

				String fullScreen =  getIntent().getStringExtra("fullScreenInd");
				if("y".equals(fullScreen)){
						getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
										WindowManager.LayoutParams.FLAG_FULLSCREEN);
						getSupportActionBar().hide();
				}


				Uri videoUri = Uri.parse(pathVideo);

				videoView.setVideoURI(videoUri);

				mediaController = new FullScreenMediaController(this);
				mediaController.setAnchorView(videoView);

				videoView.setMediaController(mediaController);
				videoView.start();

				r.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
								if (mediaController.isShown()) {
										mediaController.hide();
								}else{
										mediaController.show();
								}

						}
				});

		}




}