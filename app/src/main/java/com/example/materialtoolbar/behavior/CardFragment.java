package com.example.materialtoolbar.behavior;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.materialtoolbar.MainActivity;
import com.example.materialtoolbar.db.DaoManagerImpl;
import com.example.materialtoolbar.model.user.Account;
import com.example.materialtoolbar.model.user.Anagrafica;
import com.example.materialtoolbar.model.user.Localita;
import com.example.materialtoolbar.ricerca.LocalitaSearch;
import com.example.materialtoolbar.R;
import com.google.android.material.tabs.TabLayout;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CardFragment extends Fragment {
		private static final String ARG_COUNT = "param1";
		private static Context context1;

		private Integer counter;
		int Color = R.color.Black;
		private AdapterView.OnItemSelectedListener itemMeseSelected, itemSessoSelected, itemProvinceSelected, itemRegioneSelected;
		private long idRegione, idProvincia;

		private static Account acc1;
		private static boolean firstTime1;
		private static int caricamento;

		private static Anagrafica anagraficaUser;
		private static View viewAnag;

		public CardFragment() {
				// Required empty public constructor
		}

		public static CardFragment newInstance(Integer counter, Context context, Account acc, boolean firstTime) {
				CardFragment fragment = new CardFragment();
				Bundle args = new Bundle();
				args.putInt(ARG_COUNT, counter);
				fragment.setArguments(args);
				context1 = context;
				acc1 = acc;
				firstTime1 = firstTime;

				return fragment;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);

				if (getArguments() != null) {
						counter = getArguments().getInt(ARG_COUNT);
				}
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
														 Bundle savedInstanceState) {
				// Inflate the layout for this fragment
				if (counter == 0) {
						return inflater.inflate(R.layout.fragment_info, container, false);
				} else if (counter == 1) {
						return inflater.inflate(R.layout.fragment_localita, container, false);
				}
				return null;
		}


		@Override
		public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
				super.onViewCreated(view, savedInstanceState);

				if (counter == 0) {
						this.viewAnag = view;
						setupListenersInfo();
						Spinner spnMesi, spnSesso;
						spnMesi = view.findViewById(R.id.spn_Mese);
						spnSesso = view.findViewById(R.id.spn_Sesso);

						EditText txtCognome = view.findViewById(R.id.txt_Cognome);
						EditText txtNome = view.findViewById(R.id.txt_Nome);
						EditText txtGiorno = view.findViewById(R.id.txt_Giorno);
						EditText txtAnno = view.findViewById(R.id.txt_Anno);
						EditText txtCell = view.findViewById(R.id.txt_Numero);

						spnMesi.setOnItemSelectedListener(itemMeseSelected);
						spnSesso.setOnItemSelectedListener(itemSessoSelected);

						ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context1, R.array.sesso, android.R.layout.simple_spinner_item);
						adapter.setDropDownViewResource(R.layout.spinnerlayout);
						spnSesso.setAdapter(adapter);

						if (firstTime1) {
								spnMesi.setSelection(0);
								spnSesso.setSelection(0);
						} else {

								Dao<Anagrafica, String> anagraficaDao = null;
								try {
										anagraficaDao = DaoManagerImpl.getAnagraficaDao(context1);
										Long idUser = acc1.getId();

										List<Anagrafica> anag = anagraficaDao.queryForEq("idTab", idUser);
										if (anag.size() > 0) {
												Anagrafica utente = anag.get(0);

												txtCognome.setText(utente.getCognome());
												txtNome.setText(utente.getNome());
												txtCell.setText(utente.getCellulare());

												String dataNascita[] = utente.getDataNascita().split("/");
												txtGiorno.setText(dataNascita[0]);
												txtAnno.setText(dataNascita[2]);

												spnMesi.setSelection(Integer.parseInt(dataNascita[1]) - 1);

												if (utente.getSesso().equals("U")) {
														spnSesso.setSelection(0);
												} else {
														spnSesso.setSelection(1);
												}
										}

								} catch (SQLException e) {
										e.printStackTrace();
								}


						}


						txtNome.setOnEditorActionListener(new TextView.OnEditorActionListener() {
								@Override
								public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
										if (actionId == EditorInfo.IME_ACTION_NEXT) {
												txtGiorno.requestFocus();
												return true;
										}
										return false;
								}


						});


						txtGiorno.setOnEditorActionListener(new TextView.OnEditorActionListener() {
								@Override
								public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
										if (actionId == EditorInfo.IME_ACTION_NEXT) {
												InputMethodManager imm = (InputMethodManager) context1.getSystemService(Context.INPUT_METHOD_SERVICE);
												imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
												return true;
										}
										return false;
								}


						});
						txtAnno.setOnEditorActionListener(new TextView.OnEditorActionListener() {
								@Override
								public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
										if (actionId == EditorInfo.IME_ACTION_NEXT) {
												InputMethodManager imm = (InputMethodManager) context1.getSystemService(Context.INPUT_METHOD_SERVICE);
												imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
												return true;
										}
										return false;
								}


						});

						Button button = view.findViewById(R.id.btnSalvaInfo);
						button.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
										// TODO Auto-generated method stub
										//SalvaInfo();
										anagraficaUser = salvaAnagrafica(view);
										if (anagraficaUser != null) {
												TabLayout tabs = (TabLayout) (getActivity()).findViewById(R.id.tabLayout);
												tabs.getTabAt(1).select();
										}
								}
						});
				} else if (counter == 1) {

						ArrayList<CharSequence> regioni = new ArrayList<CharSequence>();
						ArrayAdapter<CharSequence> adapter;

						Spinner spnRegione = view.findViewById(R.id.spn_Regione);
						Spinner spnProvince = view.findViewById(R.id.spn_Province);
						AutoCompleteTextView txtComune = view.findViewById(R.id.txt_Citta);
						EditText txtIndirizzo = view.findViewById(R.id.txt_Indirizzo);
						EditText txtCap = view.findViewById(R.id.txt_Cap);

						setupListenersLocalita(spnProvince, txtComune);

						spnRegione.setOnItemSelectedListener(itemRegioneSelected);
						spnProvince.setOnItemSelectedListener(itemProvinceSelected);

						LocalitaSearch search = new LocalitaSearch(context1);


						regioni = search.getRegioni();


						adapter = new ArrayAdapter<CharSequence>(context1, android.R.layout.simple_spinner_item, regioni);
						adapter.setDropDownViewResource(R.layout.spinnerlayout);
						spnRegione.setAdapter(adapter);
						if (firstTime1 ) {
								spnRegione.setSelection(0);
								spnProvince.setSelection(0);
						}else {
								Dao<Localita, String> localitaDao = null;
								try {
										localitaDao = DaoManagerImpl.getLocalitaDao(context1);
										Long idUser = acc1.getId();

										List<Localita> anag = localitaDao.queryForEq("idTab", idUser);
										if (anag.size() > 0) {
												Localita utente = anag.get(0);

												txtComune.setText(utente.getCitta());
												txtIndirizzo.setText(utente.getIndirizzo());
												txtCap.setText(utente.getCap());



												spnRegione.setSelection(regioni.indexOf(utente.getRegione()));

												ArrayList<CharSequence> province = new ArrayList<CharSequence>();
												province = search.getProvince(search.getIDRegione(utente.getRegione()));
												caricamento = province.indexOf(utente.getProvincia());

										}
								} catch (SQLException e) {
										e.printStackTrace();
								}

						}
								txtComune.setOnEditorActionListener(new TextView.OnEditorActionListener() {
								@Override
								public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
										if (actionId == EditorInfo.IME_ACTION_NEXT) {
												txtIndirizzo.requestFocus();
												return true;
										}
										return false;
								}


						});

						Button button = view.findViewById(R.id.btnSalvaLocalita);
						button.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
										// TODO Auto-generated method stub
										try {
												SalvaInfo(view);
										} catch (SQLException e) {
												e.printStackTrace();
										}

								}
						});


				}
		}



		private void setupListenersInfo() {

				itemMeseSelected = new AdapterView.OnItemSelectedListener() {
						public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
								String item = parent.getItemAtPosition(position).toString();
								parent.setSelection(position);
						}

						@Override
						public void onNothingSelected(AdapterView<?> adapterView) {

						}
				};
				itemSessoSelected = new AdapterView.OnItemSelectedListener() {
						public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
								String item = parent.getItemAtPosition(position).toString();
								parent.setSelection(position);

						}

						@Override
						public void onNothingSelected(AdapterView<?> adapterView) {

						}
				};
		}

		private void setupListenersLocalita(Spinner spnProvince, AutoCompleteTextView txtcom) {
				itemRegioneSelected = new AdapterView.OnItemSelectedListener() {
						public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
								String item = parent.getItemAtPosition(position).toString();
								parent.setSelection(position);
								LocalitaSearch search = new LocalitaSearch(context1);

								idRegione = search.getIDRegione(item);


								ArrayList<CharSequence> province = search.getProvince(idRegione);
								Spinner spnProv = spnProvince;
								ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(context1, android.R.layout.simple_spinner_item, province);
								adapter.setDropDownViewResource(R.layout.spinnerlayout);
								spnProv.setAdapter(adapter);

						}

						@Override
						public void onNothingSelected(AdapterView<?> adapterView) {

						}
				};

				itemProvinceSelected = new AdapterView.OnItemSelectedListener() {
						public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
								String item;
								AutoCompleteTextView txt_Com = txtcom;
								if (caricamento != -1){
											parent.setSelection(caricamento);
										 item = parent.getItemAtPosition(caricamento).toString();
								}else {
										parent.setSelection(position);
										item = parent.getItemAtPosition(position).toString();
							}

								caricamento = -1;

								LocalitaSearch search = new LocalitaSearch(context1);


								idProvincia = search.getIDProvince(item);

								ArrayList<CharSequence> comuni = search.getComuni(idRegione, idProvincia);
								String comu = txt_Com.getText().toString();
								if (comuni.contains(comu) == false){
										txt_Com.setText("");
								}
								ArrayAdapter<CharSequence> adapter1 = new ArrayAdapter<CharSequence>(context1, R.layout.spinnerlayout, comuni);
								txt_Com.setAdapter(adapter1);

						/*		txt_Com.setOnItemClickListener(new AdapterView.OnItemClickListener() {
										@Override
										public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

										}
								});*/

						}

						@Override
						public void onNothingSelected(AdapterView<?> adapterView) {

						}
				};


		}

		private static Anagrafica salvaAnagrafica(View view) {
				Anagrafica utente = new Anagrafica();

				Spinner spnMesi = view.findViewById(R.id.spn_Mese);
				Spinner spnSesso = view.findViewById(R.id.spn_Sesso);


				EditText txtCognome = view.findViewById(R.id.txt_Cognome);
				EditText txtNome = view.findViewById(R.id.txt_Nome);
				EditText txtGiorno = view.findViewById(R.id.txt_Giorno);
				EditText txtAnno = view.findViewById(R.id.txt_Anno);
				EditText txtCell = view.findViewById(R.id.txt_Numero);

				Long idUser = acc1.getId();

				utente.setIdTab(idUser);

				if (txtCognome.getText().toString().equals("")) {
						return null;
				}

				if (txtNome.getText().toString().equals("")) {
						return null;
				}

				if (txtGiorno.getText().toString().equals("")) {
						return null;
				}

				if (txtAnno.getText().toString().equals("")) {
						return null;
				}

				if (txtCell.getText().toString().equals("")) {
						return null;
				}

				utente.setCognome(txtCognome.getText().toString());
				utente.setNome(txtNome.getText().toString());
				utente.setCellulare(txtCell.getText().toString());

				String mese = spnMesi.getSelectedItem().toString();

				String dataNascita = txtGiorno.getText().toString() + "/" + mese + "/" + txtAnno.getText().toString();
				utente.setDataNascita(dataNascita);



				if (spnSesso.getSelectedItemPosition() == 0) {
						utente.setSesso("U");
				} else {
						utente.setSesso("D");
				}

				return utente;
		}

		private void SalvaInfo(View view) throws SQLException {
				Localita localita = new Localita();

				Spinner spnRegione = view.findViewById(R.id.spn_Regione);
				Spinner spnProvincia = view.findViewById(R.id.spn_Province);


				AutoCompleteTextView txtComune = view.findViewById(R.id.txt_Citta);
				EditText txtIndirizzo = view.findViewById(R.id.txt_Indirizzo);
				EditText txtCap = view.findViewById(R.id.txt_Cap);

				Long idUser = acc1.getId();

				localita.setIdTab(idUser);

				if (txtComune.getText().toString().equals("")) {
						return ;
				}

				if (txtIndirizzo.getText().toString().equals("")) {
						return ;
				}

				if (txtCap.getText().toString().equals("")) {
						return ;
				}

				Dao<Anagrafica, String> anagraficaDao = DaoManagerImpl.getAnagraficaDao(context1);
				List<Anagrafica> anag = anagraficaDao.queryForEq("idTab", idUser);


				if (anag.size() >= 1) {
						UpdateBuilder<Anagrafica, String> updateBuilder = anagraficaDao.updateBuilder();

						updateBuilder.updateColumnValue("cognome", anagraficaUser.getCognome());
						updateBuilder.updateColumnValue("nome", anagraficaUser.getNome());
						updateBuilder.updateColumnValue("dataNascita", anagraficaUser.getDataNascita());
						updateBuilder.updateColumnValue("sesso", anagraficaUser.getSesso());
						updateBuilder.updateColumnValue("cellulare", anagraficaUser.getCellulare());
						updateBuilder.where().eq("idTab", idUser);

						updateBuilder.update();
				} else {
						anagraficaDao.create(anagraficaUser);
				}


				localita.setIdTab(idUser);
				localita.setCitta(txtComune.getText().toString());
				localita.setIndirizzo(txtIndirizzo.getText().toString());
				localita.setCap(txtCap.getText().toString());


				localita.setRegione(spnRegione.getSelectedItem().toString());
				localita.setProvincia(spnProvincia.getSelectedItem().toString());



				Dao<Localita, String> localitaDao = DaoManagerImpl.getLocalitaDao(context1);

				List<Localita> local = localitaDao.queryForEq("idTab", idUser);
				if (local.size() >= 1) {
						UpdateBuilder<Localita, String> updateBuilder = localitaDao.updateBuilder();

						updateBuilder.updateColumnValue("regione", localita.getRegione());
						updateBuilder.updateColumnValue("provincia", localita.getProvincia());
						updateBuilder.updateColumnValue("citta", localita.getCitta());
						updateBuilder.updateColumnValue("indirizzo", localita.getIndirizzo());
						updateBuilder.updateColumnValue("cap", localita.getCap());
						updateBuilder.where().eq("idTab", idUser);

						updateBuilder.update();
				} else {
						localitaDao.create(localita);
				}
				Toast.makeText(context1, "Salvataggio effettuato correttamente", Toast.LENGTH_LONG).show();
				if (firstTime1 == true) {
						((MainActivity) getActivity()).BackFromSignUp(null);
				}
		}

		public static void setAnagraficaUser(int position) {
				if (viewAnag != null){
						if (salvaAnagrafica(viewAnag) != null){
								if (position == 1) {
										anagraficaUser = salvaAnagrafica(viewAnag);
								}
						}
				}
		}
}
