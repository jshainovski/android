package com.example.materialtoolbar.ricerca;

import android.content.Context;

import com.example.materialtoolbar.db.DaoManagerImpl;
import com.example.materialtoolbar.model.localita.Comuni;
import com.example.materialtoolbar.model.localita.Province;
import com.example.materialtoolbar.model.localita.Regioni;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LocalitaSearch {

		Context cont;

		public LocalitaSearch(Context context){
				cont = context;

		}

		public ArrayList<CharSequence> getRegioni() {
				ArrayList<CharSequence> regioni = new ArrayList<CharSequence>();
				try {

						Dao<Regioni, String> accountDao = DaoManagerImpl.getRegioniDao(cont);
						List<Regioni> reg = accountDao.queryForAll();

						for(int i = 0 ; i < reg.size() ; i++){
								Regioni r = reg.get(i);
								String attrValue = r.getNome();

								regioni.add(attrValue);
						}



				} catch (SQLException e) {
						e.printStackTrace();
				}
				Collections.sort(regioni, new Comparator<CharSequence>() {
						@Override
						public int compare(CharSequence s1, CharSequence s2) {
								String str1 = s1.toString();
								String str2 = s2.toString();
								return str1.compareToIgnoreCase(str2);
						}
				});

				return regioni;
		}
		public ArrayList<CharSequence> getProvince(long idRegione) {
				ArrayList<CharSequence> province = new ArrayList<CharSequence>();
				try {

						Dao<Province, String> accountDao = DaoManagerImpl.getProvinceDao(cont);
						List<Province> prov = accountDao.queryForEq("id_regione",idRegione);

						for(int i = 0 ; i < prov.size()  ; i++){
								Province r = prov.get(i);
								String attrValue = r.getNome();

								province.add(attrValue);
						}



				} catch (SQLException e) {
						e.printStackTrace();
				}
				Collections.sort(province, new Comparator<CharSequence>() {
						@Override
						public int compare(CharSequence s1, CharSequence s2) {
								String str1 = s1.toString();
								String str2 = s2.toString();
								return str1.compareToIgnoreCase(str2);
						}
				});

				return province;
		}

		public ArrayList<CharSequence> getComuni(Long idRegione,Long idProvincia)  {
				ArrayList<CharSequence> comuni = new ArrayList<CharSequence>();
				try {

						Dao<Comuni, String> accountDao = DaoManagerImpl.getComuniDao(cont);
						Map<String,Object> map = new HashMap<String,Object>(){{
								put("id_regione", idRegione);
								put("id_provincia", idProvincia);
						}};
						List<Comuni> com = accountDao.queryForFieldValues(map);

						for(int i = 0 ; i < com.size()  ; i++){
								Comuni r = com.get(i);
								String attrValue = r.getNome();

								comuni.add(attrValue);
						}



				} catch (SQLException e) {
						e.printStackTrace();
				}
				Collections.sort(comuni, new Comparator<CharSequence>() {
						@Override
						public int compare(CharSequence s1, CharSequence s2) {
								String str1 = s1.toString();
								String str2 = s2.toString();
								return str1.compareToIgnoreCase(str2);
						}
				});

				return comuni;
		}


		public Long getIDRegione(String regione) {
				long idReg = 0;
				try {

						Dao<Regioni, String> accountDao = DaoManagerImpl.getRegioniDao(cont);
						List<Regioni> reg = accountDao.queryForEq("nome",regione);

						if (reg.size() > 0) {
								Regioni r = reg.get(0);
								idReg = r.getId();
						}

				} catch (SQLException e) {
						e.printStackTrace();
				}


				return idReg;
		}

		public Long getIDProvince(String provincia)  {
				long idProv = 0;
				try {

						Dao<Province, String> accountDao = DaoManagerImpl.getProvinceDao(cont);
						List<Province> reg = accountDao.queryForEq("nome",provincia);

						if (reg.size() > 0) {
								Province r = reg.get(0);
								idProv = r.getId();
						}

				} catch (SQLException e) {
						e.printStackTrace();
				}


				return idProv;
		}



}
