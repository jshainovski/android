package com.example.materialtoolbar.model.localita;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "comuni")
public class Comuni {

		@DatabaseField(id = true)
		private Long id;

		@DatabaseField
		private Long id_regione;

		@DatabaseField
		private Long id_provincia;

		@DatabaseField
		private String nome;

		@DatabaseField
		private String capoluogo_provincia;

		@DatabaseField
		private String codice_catastale;

		@DatabaseField
		private Double latitudine;

		@DatabaseField
		private Double longitudine;

		public Comuni() {
		}

		public Comuni(Long id,long id_regione,long id_provincia, String nome,String capoluogo_provincia,String codice_catastale, double latitudine, double longitudine) {
				this.id = id;
				this.id_regione = id_regione;
				this.id_provincia = id_provincia;
				this.nome = nome;
				this.capoluogo_provincia = capoluogo_provincia;
				this.codice_catastale = codice_catastale;
				this.latitudine = latitudine;
				this.longitudine = longitudine;
		}

		public Long getId() {
				return id;
		}

		public void setId(Long id) {
				this.id = id;
		}

		public Long getId_regione() {
				return id_regione;
		}

		public void setId_regione(Long id_regione) {
				this.id_regione = id_regione;
		}

		public Long getId_provincia() {
				return id_provincia;
		}

		public void setId_provincia(Long id_provincia) {
				this.id_provincia = id_provincia;
		}

		public String getNome() {
				return nome;
		}

		public void setNome(String nome) {
				this.nome = nome;
		}

		public String getCapoluogo_provincia() {
				return capoluogo_provincia;
		}

		public void setCapoluogo_provincia(String capoluogo_provincia) {
				this.capoluogo_provincia = capoluogo_provincia;
		}

		public String getCodice_catastale() {
				return codice_catastale;
		}

		public void setCodice_catastale(String codice_catastale) {
				this.codice_catastale = codice_catastale;
		}

		public Double getLatitudine() {
				return latitudine;
		}

		public void setLatitudine(Double latitudine) {
				this.latitudine = latitudine;
		}

		public Double getLongitudine() {
				return longitudine;
		}

		public void setLongitudine(Double longitudine) {
				this.longitudine = longitudine;
		}
}
