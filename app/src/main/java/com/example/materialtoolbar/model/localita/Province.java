package com.example.materialtoolbar.model.localita;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "province")
public class Province {

		@DatabaseField(id = true)
		private Long id;

		@DatabaseField
		private Long id_regione;

		@DatabaseField
		private Long codice_citta_metropolitana;

		@DatabaseField
		private String nome;

		@DatabaseField
		private String sigla_automobilistica;

		@DatabaseField
		private Double latitudine;

		@DatabaseField
		private Double longitudine;

		public Province() {
		}

		public Province(Long id,Long id_regione,Long codice_citta_metropolitana, String nome,String sigla_automobilistica, Double latitudine, Double longitudine) {
				this.id = id;
				this.id_regione = id_regione;
				this.codice_citta_metropolitana = codice_citta_metropolitana;
				this.nome = nome;
				this.sigla_automobilistica = sigla_automobilistica;
				this.latitudine = latitudine;
				this.longitudine = longitudine;
		}

		public Long getId() {
				return id;
		}

		public void setId(Long id) {
				this.id = id;
		}

		public long getId_regione() {
				return id_regione;
		}

		public void setId_regione(Long id_regione) {
				this.id_regione = id_regione;
		}

		public long getCodice_citta_metropolitana() {
				return codice_citta_metropolitana;
		}

		public void setCodice_citta_metropolitana(Long codice_citta_metropolitana) {
				this.codice_citta_metropolitana = codice_citta_metropolitana;
		}

		public String getNome() {
				return nome;
		}

		public void setNome(String nome) {
				this.nome = nome;
		}

		public String getSigla_automobilistica() {
				return sigla_automobilistica;
		}

		public void setSigla_automobilistica(String sigla_automobilistica) {
				this.sigla_automobilistica = sigla_automobilistica;
		}

		public double getLatitudine() {
				return latitudine;
		}

		public void setLatitudine(Double latitudine) {
				this.latitudine = latitudine;
		}

		public double getLongitudine() {
				return longitudine;
		}

		public void setLongitudine(Double longitudine) {
				this.longitudine = longitudine;
		}
}
