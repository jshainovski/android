package com.example.materialtoolbar.model.user;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "media")
public class Media {
		@DatabaseField(id = true)
		private Long id;

		@DatabaseField
		private Long idTab;

		@DatabaseField
		private String path;

		@DatabaseField
		private String nome;

		@DatabaseField
		private String descrizione;

		@DatabaseField
		private String data_inserimento;

		@DatabaseField
		private String ora_inserimento;

		public Media() {
		}

		public Media(Long id, Long idTab, String path, String nome, String descrizione,String data_inserimento,String ora_inserimento) {
				this.id = id;
				this.idTab = idTab;
				this.path = path;
				this.nome = nome;
				this.descrizione = descrizione;
				this.data_inserimento = data_inserimento;
				this.ora_inserimento = ora_inserimento;
		}

		public Long getId() {
				return id;
		}

		public void setId(Long id) {
				this.id = id;
		}

		public Long getIdTab() {
				return idTab;
		}

		public void setIdTab(Long idTab) {
				this.idTab = idTab;
		}

		public String getPath() {
				return path;
		}

		public void setPath(String path) {
				this.path = path;
		}

		public String getNome() {
				return nome;
		}

		public void setNome(String nome) {
				this.nome = nome;
		}

		public String getDescrizione() {
				return descrizione;
		}

		public void setDescrizione(String descrizione) {
				this.descrizione = descrizione;
		}

		public String getData_inserimento() { return data_inserimento;		}

		public void setData_inserimento(String data_inserimento) {	this.data_inserimento = data_inserimento;		}

		public String getOra_inserimento() { return ora_inserimento;		}

		public void setOra_inserimento(String ora_inserimento) { this.ora_inserimento = ora_inserimento;		}


}