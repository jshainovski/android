package com.example.materialtoolbar.model;

public class Video {
		private String video_link;
		private String nomeVideo;
		private String descrizioneVideo;

		public Video() {
		}

			public Video(String video_link, String nomeVideo, String descrizione) {
				this.video_link = video_link;
				this.nomeVideo = nomeVideo;
				this.descrizioneVideo = descrizione;
		}

		public String getVideo_link() {
				return video_link;
		}

		public void setVideo_link(String video_link) {
				this.video_link = video_link;
		}

		public String getNomeVideo() {
				return nomeVideo;
		}

		public void setNomeVideo(String nome) {
				this.nomeVideo = nome;
		}

		public String getDescrizioneVideo() {
				return descrizioneVideo;
		}

		public void setDescrizioneVideo(String descrizione) {
				this.descrizioneVideo = descrizione;
		}
}
