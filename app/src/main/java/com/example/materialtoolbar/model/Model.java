package com.example.materialtoolbar.model;

public class Model {
    private String image_link;
    private String user_image;
    private String text;
    private String text_citta;

    public Model() {
    }

    public Model(String image_link, String user_image, String text, String text_citta) {
        this.image_link = image_link;
        this.user_image = user_image;
        this.text = text;
        this.text_citta = text_citta;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser_image() { return user_image;}

    public void setUser_image(String user_image) {this.user_image = user_image;}
    public String getText_citta() {  return text_citta;    }

    public void setText_citta(String text_citta) {this.text_citta = text_citta;    }
}
