package com.example.materialtoolbar.model.user;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "localita")
public class Localita {
		@DatabaseField(id = true)
		private Long id;

		@DatabaseField
		private Long idTab;

		@DatabaseField
		private String regione;

		@DatabaseField
		private String provincia;

		@DatabaseField
		private String citta;

		@DatabaseField
		private String indirizzo;

		@DatabaseField
		private String cap;


		public Localita() {
		}

		public Localita(Long id, Long idTab, String regione, String provincia, String citta, String indirizzo, String cap) {
				this.id = id;
				this.idTab = idTab;
				this.regione = regione;
				this.provincia = provincia;
				this.citta = citta;
				this.indirizzo = indirizzo;
				this.cap = cap;
		}

		public Long getId() {
				return id;
		}

		public void setId(Long id) {
				this.id = id;
		}

		public Long getIdTab() {
				return idTab;
		}

		public void setIdTab(Long idTab) {
				this.idTab = idTab;
		}

		public String getRegione() {
				return regione;
		}

		public void setRegione(String regione) {
				this.regione = regione;
		}

		public String getProvincia() {
				return provincia;
		}

		public void setProvincia(String provincia) {
				this.provincia = provincia;
		}

		public String getCitta() {
				return citta;
		}

		public void setCitta(String citta) {
				this.citta = citta;
		}

		public String getIndirizzo() {
				return indirizzo;
		}

		public void setIndirizzo(String indirizzo) {
				this.indirizzo = indirizzo;
		}

		public String getCap() {
				return cap;
		}

		public void setCap(String cap) {
				this.cap = cap;
		}
}