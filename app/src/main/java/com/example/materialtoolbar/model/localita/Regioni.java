package com.example.materialtoolbar.model.localita;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "regioni")
public class Regioni {

		@DatabaseField(id = true)
		private Long id;

		@DatabaseField
		private String nome;

		@DatabaseField
		private Double latitudine;

		@DatabaseField
		private Double longitudine;

		public Regioni() {
		}

		public Regioni(Long id, String nome, Double latitudine, Double longitudine) {
				this.id = id;
				this.nome = nome;
				this.latitudine = latitudine;
				this.longitudine = longitudine;
		}

		public Long getId() {
				return id;
		}

		public void setId(Long id) {
				this.id = id;
		}

		public String getNome() {
				return nome;
		}

		public void setNome(String nome) {
				this.nome = nome;
		}

		public double getLatitudine() {
				return latitudine;
		}

		public void setLatitudine(Double latitudine) {
				this.latitudine = latitudine;
		}

		public double getLongitudine() {
				return longitudine;
		}

		public void setLongitudine(Double longitudine) {
				this.longitudine = longitudine;
		}
}
