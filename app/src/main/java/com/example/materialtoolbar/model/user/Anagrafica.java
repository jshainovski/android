package com.example.materialtoolbar.model.user;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "anagrafica")
public class Anagrafica {
		@DatabaseField(id = true)
		private Long id;

		@DatabaseField
		private Long idTab;

		@DatabaseField
		private String cognome;

		@DatabaseField
		private String nome;

		@DatabaseField
		private String dataNascita;

		@DatabaseField
		private String sesso;

		@DatabaseField
		private String cellulare;



		public Anagrafica() {
		}

		public Anagrafica(Long id,Long idTab, String cognome,String nome, String dataNascita, String sesso, String cellulare) {
				this.id = id;
				this.idTab = idTab;
				this.cognome = cognome;
				this.nome = nome;
				this.dataNascita = dataNascita;
				this.sesso = sesso;
				this.cellulare = cellulare;

		}

		public Long getId() {
				return id;
		}

		public void setId(Long id) {
				this.id = id;
		}

		public Long getIdTab() {
				return idTab;
		}

		public void setIdTab(Long idTab) {
				this.idTab = idTab;
		}

		public String getCognome() {
				return cognome;
		}

		public void setCognome(String cognome) {
				this.cognome = cognome;
		}

		public String getNome() {
				return nome;
		}

		public void setNome(String nome) {
				this.nome = nome;
		}

		public String getDataNascita() {
				return dataNascita;
		}

		public void setDataNascita(String dataNascita) {
				this.dataNascita = dataNascita;
		}

		public String getSesso() {
				return sesso;
		}

		public void setSesso(String sesso) {
				this.sesso = sesso;
		}

		public String getCellulare() {
				return cellulare;
		}

		public void setCellulare(String cellulare) {
				this.cellulare = cellulare;
		}

}
