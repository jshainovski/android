package com.example.materialtoolbar.adapter;

import android.content.Context;

import com.example.materialtoolbar.behavior.CardFragment;
import com.example.materialtoolbar.model.user.Account;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
public class ViewPageAdapter extends FragmentStateAdapter {
		private static final int CARD_ITEM_SIZE = 2;
		Context frag;
		Account acc;
		boolean firstTime;
		public ViewPageAdapter(@NonNull FragmentActivity fragmentActivity, Context context, Account acc,boolean firstTime) {
				super(fragmentActivity);
				frag = context;
				this.acc = acc;
				this.firstTime = firstTime;
		}
		@NonNull @Override public Fragment createFragment(int position) {
				return CardFragment.newInstance(position,frag,acc,firstTime);
		}
		@Override public int getItemCount() {
				return CARD_ITEM_SIZE;
		}




}
