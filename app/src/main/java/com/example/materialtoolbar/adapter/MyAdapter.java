package com.example.materialtoolbar.adapter;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.materialtoolbar.R;
import com.example.materialtoolbar.db.DaoManagerImpl;
import com.example.materialtoolbar.model.Model;
import com.example.materialtoolbar.model.user.Media;
import com.j256.ormlite.dao.Dao;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.yalantis.ucrop.UCropFragment.TAG;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    Context context;
    List<Model> modelList;
    private OnItemClickListener mListener;
    private int i = 0;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public MyAdapter(Context context, List<Model> modelList) {
        this.context = context;
        this.modelList = modelList;

    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.view_holder_item,viewGroup,false);
        return  new MyViewHolder(itemView, (AdapterView.OnItemClickListener) mListener);

    }



    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        if (modelList.get(i).getImage_link() != "") {
          //  Picasso.get().load(modelList.get(i).getImage_link()).into(holder.image_view);
            //Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(holder.image_view);
            Uri myUri = Uri.parse(modelList.get(i).getImage_link());
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), myUri);

                if (bitmap != null) {
                    holder.image_view.setImageBitmap(bitmap);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!modelList.get(i).getUser_image().equals("")) {
                Uri myUri = Uri.parse(modelList.get(i).getUser_image());
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), myUri);

                if (bitmap != null) {
                    holder.img_profile_photo.setImageBitmap(bitmap);
                }
            } catch (IOException e) {
                e.printStackTrace();
                holder.img_profile_photo.setImageResource(R.drawable.img2);
            }
        }else{
            holder.img_profile_photo.setImageResource(R.drawable.img2);
        }

        holder.text_view.setText(modelList.get(i).getText());
        holder.text_view_citta.setText(modelList.get(i).getText_citta());
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView image_view;
        public TextView text_view;
        public TextView text_view_citta;
        public com.mikhaellopez.circularimageview.CircularImageView img_profile_photo;

        public void changeText1(String text){
            text_view.setText(text);
        }

        public MyViewHolder(@NonNull View itemView, final AdapterView.OnItemClickListener listener) {
            super(itemView);

            image_view = itemView.findViewById(R.id.image_view);
            text_view = itemView.findViewById(R.id.text_view);
            text_view_citta = itemView.findViewById(R.id.text_view_citta);
            img_profile_photo = itemView.findViewById(R.id.img_profile_photo);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView tv = v.findViewById(R.id.text_view);
                    Activity a = (Activity) context;
                    //a.setContentView(R.layout.login);
                }
            });
        }
    }
}
