package com.example.materialtoolbar.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.materialtoolbar.R;
import com.example.materialtoolbar.model.Model;
import com.example.materialtoolbar.model.Video;
import com.example.materialtoolbar.video.Video1;
import com.yalantis.ucrop.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyVideoAdapter extends RecyclerView.Adapter<MyVideoAdapter.MyViewHolderVideo> {
		public static final String EXTRA_MESSAGE = "com.example.materialtoolbar.adapter.MESSAGE";

		Context context;
		List<Video> videoList;
		private OnItemClickListener mListener;
		private int i = 0;

		public interface OnItemClickListener{
				void onItemClick(int position);
		}

		public void setOnItemClickListener(OnItemClickListener listener){
				mListener = listener;
		}

		public MyVideoAdapter(Context context, List<Video> videoList) {
				this.context = context;
				this.videoList = videoList;

		}



		@NonNull
		@Override
		public MyViewHolderVideo onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
				View itemView = LayoutInflater.from(context).inflate(R.layout.video_holder_item,viewGroup,false);
				return  new MyViewHolderVideo(itemView, (AdapterView.OnItemClickListener) mListener);

		}



		@Override
		public void onBindViewHolder(@NonNull MyViewHolderVideo holder, int i) {
				if (videoList.get(i).getVideo_link() != "") {
						//  Picasso.get().load(modelList.get(i).getImage_link()).into(holder.image_view);
						//Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(holder.image_view);

						//Uri myUri = Uri.parse(videoList.get(i).getVideo_link());
						Bitmap bitmap = createVideoThumbNail(videoList.get(i).getVideo_link());
						holder.video_view.setImageBitmap(bitmap);
						holder.video_view.setTag(videoList.get(i).getVideo_link());
				}

				holder.text_video.setText(videoList.get(i).getNomeVideo());
				holder.text_video_descr.setText(videoList.get(i).getDescrizioneVideo());
		}

		public Bitmap createVideoThumbNail(String path){
				String correctedUri = FileUtils.getPath(context, Uri.parse(path));

				Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(correctedUri, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
				return bitmap;
		}

		@Override
		public int getItemCount() {
				return videoList.size();
		}

		public class MyViewHolderVideo extends RecyclerView.ViewHolder{
				public ImageView video_view;
				public TextView text_video;
				public TextView text_video_descr;
				public Button btnMenuVideo;

				public void changeText1(String text){
						text_video.setText(text);
				}

				public MyViewHolderVideo(@NonNull View itemView, final AdapterView.OnItemClickListener listener) {
						super(itemView);

						video_view = itemView.findViewById(R.id.video_view);
						text_video = itemView.findViewById(R.id.text_video);
						text_video_descr = itemView.findViewById(R.id.text_video_descr);
						btnMenuVideo = itemView.findViewById(R.id.btnMenuVideo);


						btnMenuVideo.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
										FrameLayout menu = itemView.findViewById(R.id.menu_video);

										if (menu.getVisibility() == View.VISIBLE) {
												// Its visible
												menu.setVisibility(View.INVISIBLE);
										} else {
												// Either gone or invisible
												menu.setVisibility(View.VISIBLE);
										}

								}
						});

						video_view.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
										Object path = v.getTag();
										Intent intent = new Intent(context, Video1.class);
										intent.putExtra(EXTRA_MESSAGE, path.toString());
										context.startActivity(intent);


								}
						});
				}
		}
}

